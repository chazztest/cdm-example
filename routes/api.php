<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/car_model/{id}', function (Request $request, $id) {
    return response()->json(DB::table('car_models')->where('make_id',(int)$id)->get());
})->middleware('auth:api');

Route::post('/stock/{stock_id}/image/{img_id}','StockImageController@destroy')->middleware('auth:api');

Route::get('/stock/lookup/{plate_num}', 'StockController@lookup')->middleware('auth:api');
