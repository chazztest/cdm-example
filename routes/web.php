<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/
Auth::routes();

Route::get('/subscriptions', 'SubscriptionController@index')->middleware('not_subscribed');
Route::get('/subscriptions/renew', 'SubscriptionController@renew');
Route::post('/subscriptions/renew_proceed', 'SubscriptionController@renew_proceed');
Route::post('/subscribe', 'SubscriptionController@subscribe');

Route::group(['middleware'=> ['auth:web', 'admins', 'subscriptions']], function() {


    Route::get('/settings/edit', [
       'as' => 'user_settings.edit',
       'uses' => 'UserController@user_edit'
    ]);

    Route::post('/settings/update',[
        'as'=> 'user_settings.update',
        'uses' => 'UserController@user_update'
    ]);

    Route::get('/permissions', 'HomeController@permissions');

    Route::get('/', 'DiaryController@index');
    Route::post('/diary_store', 'DiaryController@store');
    Route::post('/diary_update/{id}', 'DiaryController@update');
    Route::post('/diary_date_move', 'DiaryController@diary_date_move');
    Route::post('/diary_remove/{id}', 'DiaryController@destroy');

    Route::get('/users_view', 'UserController@index')->middleware('read');
    Route::get('/register_view', 'UserController@register_view')->middleware('create');
    Route::post('/store', 'UserController@store')->middleware('create');
    Route::get('/user_edit/{id}', 'UserController@edit')->middleware('update');
    Route::post('/update', 'UserController@update')->middleware('update');
    Route::get('/user_show/{id}', 'UserController@show')->middleware('read');
    Route::get('/user_delete_stock/{id}', 'UserController@destroy')->middleware('delete');
    Route::get('/user_delete/{id}', 'UserController@destroy_user')->middleware('delete');


    Route::get('/stock', 'StockController@index')->middleware('read_stock');
    Route::get('/stock/grid','StockController@index_grid')->middleware('read_stock');


    Route::get('/stock/create/{step}', 'StockController@create')->where(['step' => '[1-3]'])->middleware('add_stock');

    Route::post('/stock/store', 'StockController@store')->middleware('add_stock');
    Route::get('/stock/show/{id}', 'StockController@show')->middleware('read_stock');
    Route::get('/stock/edit/{id}', 'StockController@edit')->middleware('edit_stock');
    Route::post('/stock/update/{id}', 'StockController@update')->middleware('edit_stock');
    Route::get('/stock/delete/{id}', 'StockController@destroy')->middleware('delete_stock');
    Route::get('/stock/reorder-images/{id}', 'StockController@reorderImgShow')->middleware('edit_stock');
    Route::post('/stock/images/{id}/save', 'StockImageController@reorderImgSave')->middleware('edit_stock','auth:api');

    Route::get('/download/pdf/{id}', 'StockController@downloadPDF');


    Route::get('/locations', 'LocationController@index');
    Route::get('/locations_create', 'LocationController@create');
    Route::post('/locations_store', 'LocationController@store');
    Route::get('/locations_edit/{id}', 'LocationController@edit');
    Route::post('/locations_update', 'LocationController@update');
    Route::get('/locations_delete/{id}', 'LocationController@destroy');

    Route::get('/packages', 'PackageController@index');
    Route::get('/package/create', 'PackageController@create');
    Route::post('/package/store', 'PackageController@store');
    Route::get('/package/edit/{id}', 'PackageController@edit');
    Route::post('/package/update/{id}', 'PackageController@update');
    Route::get('/package/delete/{id}', 'PackageController@destroy');

    Route::get('logs/webhooks', 'LogController@webhooks');
    Route::get('/subscriptions/invoices', 'SubscriptionController@invoices')->middleware('view_invoices');
    Route::get('/subscriptions/invoice/{invoice}', 'SubscriptionController@download_invoice')->middleware('billing');
    Route::get('/subscriptions/change', 'SubscriptionController@change');
    Route::post('/subscriptions/change_proceed', 'SubscriptionController@change_proceed');
    Route::get('/subscriptions/cancel', 'SubscriptionController@cancel');
    Route::get('/subscriptions/reactivate', 'SubscriptionController@reactivate');

    Route::get('/car_history', 'CarHistoryController@index');
    Route::get('/car_history/create', 'CarHistoryController@create');
    Route::post('/car_history/store', 'CarHistoryController@store');
    Route::get('/car_history/edit/{id}', 'CarHistoryController@edit');
    Route::post('/car_history/update/{id}', 'CarHistoryController@update');
    Route::get('/car_history/delete/{id}', 'CarHistoryController@destroy');

    // ................ //

  