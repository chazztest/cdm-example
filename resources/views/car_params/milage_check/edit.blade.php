@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row">
        <div class="col-sm-8 col-sm-offset-1 panel-block">
            <div class="row p-header">
                <div class="col-xs-12"><h2>Edit Milage Check</h2></div>
            </div>

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/milage/' . $milage->id ) }}">
                <input name="_method" type="hidden" value="PUT">
                <div class="row p-content">
                    <div class="col-xs-12">
                        Milage check name
                    </div>
                    {{ csrf_field() }}
                    <div class="col-xs-12">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="col-xs-12">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $source->name }}" placeholder="Enter" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 submit-block">

                        <button type="submit" class="btn form-control">
                            <span class="glyphicon glyphicon-edit"> Edit
                    </div>
                </div>
            </form>
        </div>        
    </div>            
</div>
@endsection