@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row">
        <div class="col-sm-8 col-sm-offset-1 panel-block">
            <div class="row p-header">
                <div class="col-xs-12"><h2>Enter a new model for <b>{{ $car_make->name }}</b></h2></div>
            </div>

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/car_model' ) }}">
                <div class="row p-content">
                    <div class="col-xs-12">
                        Vehicle model
                    </div>
                    {{ csrf_field() }}

                    <input type="hidden" name="make_id" value="{{ $car_make->id }}">

                    <div class="col-xs-12">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="col-xs-12">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Enter"  autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-xs-12 submit-block">

                        <button type="submit" class="btn form-control">
                            <span class="glyphicon glyphicon-edit"> Create
                    </div>
                </div>
            </form>
        </div>        
    </div>            
</div>
@endsection
