@extends('layouts.app')

@section('content')
    <div class="container-fluid responsive-table-block">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                <div class="table-responsive">
                    <div class="t-header">
                        <div class="t-title">Color</div>
                        <a href="/color/create"><div class="t-action">Add new color <span class="glyphicon glyphicon-folder-open"></span></div></a>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th colspan="2"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($colors as $key => $color)
                            <tr>
                                <td scope="row">{{ $color->id }}</td>
                                <td>{{ $color->name }}</td>
                                <td>
                                    <div class="actions">
                                        <div class="action-trash"><a href="/color/delete/{{ $color->id }}"><span class="glyphicon glyphicon-trash span-bordered default-color"></span></a></div>
                                    </div>
                                </td>
                                <td>
                                    <div class="actions">
                                        <div class="action-edit"><a href="color/{{ $color->id }}/edit/"><span class="span-bordered default-color">Edit</span></a></div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection