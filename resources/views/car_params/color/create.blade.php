@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row">
        <div class="col-sm-8 col-sm-offset-1 panel-block">
            <div class="row p-header">
                <div class="col-xs-12"><h2>Create a new Color</h2></div>
            </div>

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/color' ) }}">
                <div class="row p-content">
                    <div class="col-xs-12">
                        Color name
                    </div>
                    {{ csrf_field() }}
                    <div class="col-xs-12">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="col-xs-12">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Enter" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 submit-block">

                        <button type="submit" class="btn form-control">
                            <span class="glyphicon glyphicon-edit"> Create
                    </div>
                </div>
            </form>
        </div>        
    </div>            
</div>
@endsection
