@extends('layouts.app')

@section('content')
    <div class="container-fluid responsive-table-block">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                <div class="table-responsive">
                    <div class="t-header">
                        <div class="t-title">Vehicle makes</div>
                        <a class="btn-blue add pull-right" href="/car_make/create/{{ $vehicle_type_id }}">Add new vehicle make</a>

                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th colspan="2">
                                <form action="/car_make" method="GET" class="form-horizontal" role="filter">
                                    {{ csrf_field() }}
                                    <div class="form-group-cmd">
                                            <button class="btn-blue pull-right">Select vehicle type</button>
                                        <div class="col-md-5 col-xs-10 pull-right">
                                            <select name="vehicle_type_id" id="filter" class="form-control">
                                                <option value="1" @if($vehicle_type_id == 1) selected = "selected" @endif >Cars</option>
                                                <option value="2" @if($vehicle_type_id == 2) selected = "selected" @endif>Bikes</option>
                                                <option value="3" @if($vehicle_type_id == 3) selected = "selected" @endif>Vans</option>
                                            </select>
                                        </div>

                                    </div>
                                </form>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($car_makes as $key => $car_make)
                            <tr>
                                <td scope="row">{{ $car_make['id'] }}</td>
                                <td><a href="/car_model/{{ $car_make['id'] }}">{{ $car_make['name'] }}</a></td>
                                <td>
                                    <div class="actions del-edit-btn">
                                        <a href="car_make/{{ $car_make['id'] }}/edit/"><span class="span-bordered default-color">Edit</span></a>
                                        <a href="/car_make/delete/{{ $car_make['id'] }}"><span class="glyphicon glyphicon-trash span-bordered default-color"></span></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection