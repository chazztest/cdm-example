@extends('layouts.app')

@section('content')
    <div class="container-fluid responsive-table-block">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                <div class="table-responsive">
                    <div class="t-header">
                        <div class="t-title">{{ $vehicle_type_name }}</div>
                        <a href="/car_make/create/{{ $vehicle_type_id }}"><div class="t-action">Add new vehicle make <span class="glyphicon glyphicon-folder-open"></span></div></a>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th colspan="2">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($vehicle_makes as $key => $vehicle_make)
                            <tr>
                                <td scope="row">{{ $vehicle_make['id'] }}</td>
                                <td><a href="/car_model/{{ $vehicle_make['id'] }}">{{ $vehicle_make['name'] }}</a></td>
                                <td>
                                    <div class="actions">
                                        <div class="action-trash"><a href="/car_make/delete/{{ $vehicle_make['id'] }}"><span class="glyphicon glyphicon-trash span-bordered default-color"></span></a></div>
                                    </div>
                                </td>
                                <td>
                                    <div class="actions">
                                        <div class="action-edit"><a href="/car_make/{{ $vehicle_make['id'] }}/edit/"><span class="span-bordered default-color">Edit</span></a></div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection