@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row">
        <div class="col-sm-8 col-sm-offset-1 panel-block">
            <div class="row p-header">
                <div class="col-xs-12"><h2>Create a new transmission</h2></div>
            </div>

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/transmission/store' ) }}">
                <div class="row p-content">
                    <div class="col-xs-12">
                        Transmission name
                    </div>
                    {{ csrf_field() }}
                    <div class="col-xs-12">
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <div class="col-xs-12">
                                <input id="type" type="text" class="form-control" name="type" value="{{ old('type') }}" placeholder="Enter" required autofocus>
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 submit-block">
                        <button type="submit" class="btn form-control">
                            <span class="glyphicon glyphicon-edit"> Create
                    </div>
                </div>
            </form>
        </div>        
    </div>            
</div>
@endsection
