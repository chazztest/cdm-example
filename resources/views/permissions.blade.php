@extends('layouts.app')

@section('content')
<script>
     var permissions = <?php echo json_encode($permissions); ?>;
</script>
<div class="container">

    <div class="row">
        <div class="col-md-8 col-md-offset-2 user-panel">
            <div class="row p-header">
                <div class="col-xs-12"><h2>Current user permissions</h2></div>
            </div>

                <div class="row p-content">

                        <div class="col-xs-12">
                            Permissions
                        </div>


                        <div class="col-xs-12">
                            <div class="form-group">
                                <div id="permsisions-block">

                                    @foreach($permissions as $perm => $value)
                                        <div class="col-xs-6">
                                            <input type="checkbox" name="permissions[]" value="{{ $perm }}" @if($value) checked @endif>{{ $perm }}
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                </div>

        </div>
    </div>
</div>
@endsection
