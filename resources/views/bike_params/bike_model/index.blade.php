@extends('layouts.app')

@section('content')
    <div class="container-fluid responsive-table-block">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                <div class="table-responsive">
                    <div class="t-header">
                        <div class="t-title">Bike Makes</div>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bike_makes_models as $key => $bike_make)
                            <tr>
                                <td scope="row">{{ $bike_make->id }}</td>
                                <td><a href="{{ url('/bike_model/'. $bike_make->id) }}">{{ $bike_make->name }}</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection