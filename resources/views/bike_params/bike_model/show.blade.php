@extends('layouts.app')

@section('content')
<div class="container-fluid responsive-table-block">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
            <div class="table-responsive">
                <div class="t-header">
                    <div class="t-title">Bike make: <em>{{ $bike_make->name }}</em></div>
                    <a href="/bike_model/create/{{ $bike_make->id }}"><div class="t-action">Add new vehicle model <span class="glyphicon glyphicon-folder-open"></span></div></a>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th colspan="2"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bike_models as $key => $bike_model)
                    <tr>
                        <td scope="row">{{ $bike_model->id }}</td>
                        <td>{{ $bike_model->name }}</td>
                        <td>
                            <div class="actions">
                                <div class="action-trash"><a href="/bike_model/delete/{{ $bike_model->id }}"><span class="glyphicon glyphicon-trash span-bordered default-color"></span></a></div>
                            </div>
                        </td>
                        <td>
                            <div class="actions">
                                <div class="action-edit"><a href="/bike_model/{{ $bike_model->id }}/edit/"><span class="span-bordered default-color">Edit</span></a></div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection