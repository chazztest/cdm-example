<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            [
                'path' => '1/',
                'name' => 'Andy',
                'email' => 'andy_fake@code.com',
                'password' => bcrypt('coding'),
                'level' => 'admin',
                'api_token' => str_random(60),
                'permissions' => 'a:29:{s:6:"create";b:1;s:4:"read";b:1;s:6:"update";b:1;s:6:"delete";b:1;s:14:"change_website";b:1;s:10:"read_stock";b:1;s:9:"add_stock";b:1;s:10:"edit_stock";b:1;s:12:"create_stock";b:1;s:12:"update_stock";b:1;s:12:"delete_stock";b:1;s:10:"sell_stock";b:1;s:12:"confirm_sale";b:1;s:12:"cancel_order";b:1;s:16:"location_manager";b:1;s:12:"action_diary";b:1;s:14:"invoicing_view";b:1;s:14:"invoicing_send";b:1;s:15:"customer_create";b:1;s:13:"customer_read";b:1;s:15:"customer_update";b:1;s:15:"customer_delete";b:1;s:20:"customer_prospecting";b:1;s:11:"report_sale";b:1;s:13:"report_market";b:1;s:12:"report_stock";b:1;s:9:"docs_user";b:1;s:14:"docs_edu_video";b:1;s:20:"subscription_manager";b:1;}',
                'superadmin' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'path' => '2/',
                'name' => 'Admin',
                'email' => 'admin_fake@coding.com',
                'password' => bcrypt('coding'),
                'level' => 'admin',
                'api_token' => str_random(60),
                'permissions' => 'a:29:{s:6:"create";b:1;s:4:"read";b:1;s:6:"update";b:1;s:6:"delete";b:1;s:14:"change_website";b:1;s:10:"read_stock";b:1;s:9:"add_stock";b:1;s:10:"edit_stock";b:1;s:12:"create_stock";b:1;s:12:"update_stock";b:1;s:12:"delete_stock";b:1;s:10:"sell_stock";b:1;s:12:"confirm_sale";b:1;s:12:"cancel_order";b:1;s:16:"location_manager";b:1;s:12:"action_diary";b:1;s:14:"invoicing_view";b:1;s:14:"invoicing_send";b:1;s:15:"customer_create";b:1;s:13:"customer_read";b:1;s:15:"customer_update";b:1;s:15:"customer_delete";b:1;s:20:"customer_prospecting";b:1;s:11:"report_sale";b:1;s:13:"report_market";b:1;s:12:"report_stock";b:1;s:9:"docs_user";b:1;s:14:"docs_edu_video";b:1;s:20:"subscription_manager";b:1;}',
                'superadmin' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'path' => '1/2/3/',
                'name' => 'Dealer principle',
                'email' => 'dealer_principle@coding.com',
                'password' => bcrypt('coding'),
                'level' => 'dealer_principle',
                'api_token' => str_random(60),
                'permissions' => 'a:29:{s:6:"create";b:1;s:4:"read";b:1;s:6:"update";b:1;s:6:"delete";b:0;s:14:"change_website";b:1;s:10:"read_stock";b:1;s:9:"add_stock";b:1;s:10:"edit_stock";b:1;s:12:"create_stock";b:1;s:12:"update_stock";b:1;s:12:"delete_stock";b:1;s:10:"sell_stock";b:1;s:12:"confirm_sale";b:1;s:12:"cancel_order";b:1;s:16:"location_manager";b:1;s:12:"action_diary";b:1;s:14:"invoicing_view";b:1;s:14:"invoicing_send";b:1;s:15:"customer_create";b:1;s:13:"customer_read";b:1;s:15:"customer_update";b:1;s:15:"customer_delete";b:1;s:20:"customer_prospecting";b:1;s:11:"report_sale";b:1;s:13:"report_market";b:1;s:12:"report_stock";b:1;s:9:"docs_user";b:1;s:14:"docs_edu_video";b:1;s:20:"subscription_manager";b:0;}',
                'superadmin' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'path' => '1/2/4/',
                'name' => 'Sales manager',
                'email' => 'sales_manager@coding.com',
                'password' => bcrypt('coding'),
                'level' => 'sales_manager',
                'api_token' => str_random(60),
                'permissions' => 'a:29:{s:6:"create";b:1;s:4:"read";b:1;s:6:"update";b:1;s:6:"delete";b:0;s:14:"change_website";b:0;s:10:"read_stock";b:1;s:9:"add_stock";b:1;s:10:"edit_stock";b:1;s:12:"create_stock";b:1;s:12:"update_stock";b:1;s:12:"delete_stock";b:1;s:10:"sell_stock";b:1;s:12:"confirm_sale";b:1;s:12:"cancel_order";b:1;s:16:"location_manager";b:0;s:12:"action_diary";b:1;s:14:"invoicing_view";b:1;s:14:"invoicing_send";b:1;s:15:"customer_create";b:1;s:13:"customer_read";b:1;s:15:"customer_update";b:1;s:15:"customer_delete";b:1;s:20:"customer_prospecting";b:1;s:11:"report_sale";b:1;s:13:"report_market";b:1;s:12:"report_stock";b:0;s:9:"docs_user";b:1;s:14:"docs_edu_video";b:1;s:20:"subscription_manager";b:0;}',
                'superadmin' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'path' => '2/5/',
                'name' => 'Sam man',
                'email' => 'sam@coding.com',
                'password' => bcrypt('coding'),
                'level' => 'sales_man',
                'api_token' => str_random(60),
                'permissions' => 'a:29:{s:6:"create";b:0;s:4:"read";b:0;s:6:"update";b:0;s:6:"delete";b:0;s:14:"change_website";b:0;s:10:"read_stock";b:1;s:9:"add_stock";b:1;s:10:"edit_stock";b:0;s:12:"create_stock";b:1;s:12:"update_stock";b:1;s:12:"delete_stock";b:1;s:10:"sell_stock";b:1;s:12:"confirm_sale";b:0;s:12:"cancel_order";b:0;s:16:"location_manager";b:0;s:12:"action_diary";b:1;s:14:"invoicing_view";b:0;s:14:"invoicing_send";b:0;s:15:"customer_create";b:1;s:13:"customer_read";b:1;s:15:"customer_update";b:1;s:15:"customer_delete";b:1;s:20:"customer_prospecting";b:1;s:11:"report_sale";b:0;s:13:"report_market";b:0;s:12:"report_stock";b:0;s:9:"docs_user";b:1;s:14:"docs_edu_video";b:1;s:20:"subscription_manager";b:0;}',
                'superadmin' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'path' => '2/6/',
                'name' => 'Dan man',
                'email' => 'dany@coding.com',
                'password' => bcrypt('coding'),
                'level' => 'sales_man',
                'api_token' => str_random(60),
                'permissions' => 'a:29:{s:6:"create";b:0;s:4:"read";b:0;s:6:"update";b:0;s:6:"delete";b:0;s:14:"change_website";b:0;s:10:"read_stock";b:1;s:9:"add_stock";b:1;s:10:"edit_stock";b:0;s:12:"create_stock";b:1;s:12:"update_stock";b:1;s:12:"delete_stock";b:1;s:10:"sell_stock";b:1;s:12:"confirm_sale";b:0;s:12:"cancel_order";b:0;s:16:"location_manager";b:0;s:12:"action_diary";b:1;s:14:"invoicing_view";b:0;s:14:"invoicing_send";b:0;s:15:"customer_create";b:1;s:13:"customer_read";b:1;s:15:"customer_update";b:1;s:15:"customer_delete";b:1;s:20:"customer_prospecting";b:1;s:11:"report_sale";b:0;s:13:"report_market";b:0;s:12:"report_stock";b:0;s:9:"docs_user";b:1;s:14:"docs_edu_video";b:1;s:20:"subscription_manager";b:0;}',
                'superadmin' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'path' => '2/7/',
                'name' => 'Ben man',
                'email' => 'ben@coding.com',
                'password' => bcrypt('coding'),
                'level' => 'sales_man',
                'api_token' => str_random(60),
                'permissions' => 'a:29:{s:6:"create";b:0;s:4:"read";b:0;s:6:"update";b:0;s:6:"delete";b:0;s:14:"change_website";b:0;s:10:"read_stock";b:1;s:9:"add_stock";b:1;s:10:"edit_stock";b:0;s:12:"create_stock";b:1;s:12:"update_stock";b:1;s:12:"delete_stock";b:1;s:10:"sell_stock";b:1;s:12:"confirm_sale";b:0;s:12:"cancel_order";b:0;s:16:"location_manager";b:0;s:12:"action_diary";b:1;s:14:"invoicing_view";b:0;s:14:"invoicing_send";b:0;s:15:"customer_create";b:1;s:13:"customer_read";b:1;s:15:"customer_update";b:1;s:15:"customer_delete";b:1;s:20:"customer_prospecting";b:1;s:11:"report_sale";b:0;s:13:"report_market";b:0;s:12:"report_stock";b:0;s:9:"docs_user";b:1;s:14:"docs_edu_video";b:1;s:20:"subscription_manager";b:0;}',
                'superadmin' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'path' => '',
                'name' => 'Super Admin',
                'email' => 'super@coding.com',
                'password' => bcrypt('coding'),
                'level' => '',
                'api_token' => str_random(60),
                'permissions' => null,
                'superadmin' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);

        DB::table('locations')->insert([
            ['name' => 'Diamond cars', 'user_id' => 2, 'address' => '300-302 Twyford Abbey Rd, London NW10 7DD, United kingdom','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')],
            ['name' => 'Golden cars', 'user_id' => 2, 'address' => '7 Wincobank avenue, Sheffield S56BB, United kingdom','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')],
            ['name' => 'Royal cars', 'user_id' => 2, 'address' => '49 John Lennon street, Liverpool, United kingdom','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        ]);
/*
        DB::table('users_locations')->insert([
            ['user_id' => 3, 'location_id' => 1],
            ['user_id' => 4, 'location_id' => 2],
            ['user_id' => 5, 'location_id' => 2],
            ['user_id' => 6, 'location_id' => 1],
            ['user_id' => 7, 'location_id' => 1]
        ]);
*/
        DB::table('packages')->insert([
            [
                'name' => 'Basic',
                'stripe_plan' => 'basic',
                'price' => '20',
                'billing_period' => 1,
                'trial_period' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Silver',
                'stripe_plan' => 'silver',
                'price' => '30',
                'billing_period' => 1,
                'trial_period' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Golden',
                'stripe_plan' => 'golden',
                'price' => '40',
                'billing_period' => 1,
                'trial_period' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ]);

        DB::table('vehicle_types')->insert([
            ['id' => 1, 'name' => 'Cars'],
            ['id' => 2,'name' => 'Bikes'],
            ['id' => 3,'name' => 'Vans']
        ]);
    }
}
