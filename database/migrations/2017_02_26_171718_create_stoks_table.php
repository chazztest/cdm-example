<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('vehicle_type_id')->unsigned();
            $table->foreign('vehicle_type_id')->references('id')->on('vehicle_types')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('make_id')->unsigned();
            $table->foreign('make_id')->references('id')->on('car_makes')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('model_id')->unsigned()->nullable();
            $table->foreign('model_id')->references('id')->on('car_models')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('color_id')->unsigned()->nullable();
            $table->foreign('color_id')->references('id')->on('colors')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('location_id')->unsigned()->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('vat_type_id')->unsigned()->nullable();
            $table->foreign('vat_type_id')->references('id')->on('vat_types')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('sale_type_id')->unsigned()->nullable();
            $table->foreign('sale_type_id')->references('id')->on('sale_types')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('status_id')->unsigned()->nullable();
            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('milage_check_id')->unsigned()->nullable();
            $table->foreign('milage_check_id')->references('id')->on('milage_checks')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('source_id')->unsigned()->nullable();
            $table->foreign('source_id')->references('id')->on('sources')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('vehicle_history_id')->unsigned()->nullable();
            $table->foreign('vehicle_history_id')->references('id')->on('car_histories')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('warranty_id')->unsigned()->nullable();
            $table->foreign('warranty_id')->references('id')->on('warranties')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('fuel_type_id')->unsigned()->nullable();
            $table->foreign('fuel_type_id')->references('id')->on('fuel_types')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('transmission_id')->unsigned()->nullable();
            $table->foreign('transmission_id')->references('id')->on('transmissions')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('body_type_id')->unsigned()->nullable();
            $table->foreign('body_type_id')->references('id')->on('body_types')->onDelete('cascade')->onUpdate('cascade');

            $table->string('reg_number', 25);
            $table->decimal('actual_milage', 10, 2);
            $table->date('reg_date');
            
            $table->date('mot_date');
            $table->integer('engine_number');
            $table->decimal('retail_price', 10, 2);
            $table->string('vin_num');
            $table->unsignedTinyInteger('ex_owners_count');
            $table->unsignedTinyInteger('ex_owners_show');
            
            $table->unsignedTinyInteger('finance_show');
            $table->unsignedTinyInteger('insurance_group');
            $table->decimal('purchase_price', 10, 2);
            $table->date('purchase_date');
            $table->string('supplier_inv_no');
            $table->string('supplier_name');
            $table->string('v5_number');
            $table->string('v5_type');

            $table->longText('stock_comment');
            $table->integer('engine_size_cc');
            $table->decimal('engine_size_liters', 4,2 );
            $table->string('drive', 45);
            $table->unsignedTinyInteger('doors');
            $table->decimal('siv_value', 10, 2);
            $table->string('trim_type');
            $table->string('trim_color');

            $table->longText('service_comment');
            $table->longText('retail_info');
            $table->longText('strapline');
            $table->longText('key_info');
            $table->longText('additional_info');
            $table->string('media_link');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stocks');
    }
}
