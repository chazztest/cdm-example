<?php
namespace App\Classes;

use Illuminate\Support\Facades\Auth;
use App\CarModel as CarModel;
use DB;

class StockService {

    public function get_body_types($vehicle_type_id)
    {
        return DB::table('body_types')->where('vehicle_type_id', (int)$vehicle_type_id)->get();
    }

    public function get_user_locations($user)
    {
        $user_ids = array_filter(explode('/', $user->path));

        if( count($user_ids) > 0){
            return DB::table('locations')->whereIn('user_id', $user_ids)->get();
        }

        return false;
    }

    public function get_models_by_id($id)
    {
        return (DB::table('car_models')->where('make_id',(int)$id)->get());
    }

    public function get_models_by_user_make_id($make_id){
        $user_id = Auth::user()->id;
        return (DB::table('car_models')->where('make_id',(int)$make_id)->orWhere('user_id', $user_id)->get());
    }

    public function processLookup($lookupData, $vehicle_type_id){

        $make = DB::table('car_makes')->where('name', 'like', '%' . $lookupData['make'] . '%')->where('vehicle_type_id', (int)$vehicle_type_id )->first();
        if( count($make) > 0 )
        {
            $stock_data = [];
            $stock_data['make_id'] = $make->id;

            $user = Auth::user();

            $model = DB::table('car_models')->where('name', 'like', '%' . $lookupData['model'] . '%')->where('make_id',$make->id)->first();

            if( !isset($model) or count($model) < 1 )
            {
                $stock_data['model_id'] = CarModel::create([
                    'name' => $lookupData['model'],
//                    'user_id' => (int)$user->id,
                    'make_id' => (int)$make->id
                ])->id;
            }else{
                $stock_data['model_id'] = $model->id;
            }

            $color = DB::table('colors')->where('name', 'like', '%' . $lookupData['colour'] . '%')->first();

            if(isset($color) and count($color) > 0)
            {
                $stock_data['color_id'] = $color->id;
            }

            $fuel_type = DB::table('fuel_types')->where('type', 'like', '%' . $lookupData['fuelType'] . '%')->first();

            if(isset($fuel_type) and count($fuel_type) > 0)
            {
                $stock_data['fuel_type_id'] = $fuel_type->id;
            }

            $transmission = DB::table('transmissions')->where('type', 'like', '%' . $lookupData['transmission'] . '%')->first();

            if( isset($transmission) and count($transmission) > 0 )
            {
                $stock_data['transmission_id'] = $transmission->id;
            }

            $stock_data['reg_date'] = date('d-m-Y', strtotime($lookupData['dateOfFirstRegistration']));
            $stock_data['vin_num'] = $lookupData['vin'];
            $stock_data['doors'] = $lookupData['numberOfDoors'];
            $stock_data['mot_date'] = date('d-m-Y', strtotime(preg_replace('/Expires: /', '', $lookupData['motDetails'])));
            $stock_data['engine_size_cc'] = preg_replace('/\D/', '', $lookupData['cylinderCapacity']);

            return $stock_data;
        }

        return false;
    }
}