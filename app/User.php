<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use App\Package;
use App\Subscription;

class User extends Authenticatable
{
    use Notifiable;
    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'initials', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function vehicles(){
        return $this->hasMany('App\Vehicle', 'user_id');
    }


    public function get_permissions()
	{

        if(isset($this->attributes["permissions"])){
            return unserialize($this->attributes["permissions"]);
        }

        return false;
	}

	public function is_subscribed(){

        $packages = Package::all();

        $parent = $this->get_top_most_user();

        foreach($packages as $package){

            if ($this->subscribed($package->name)){
                return true;
            }
        }

        foreach($packages as $package){
            if ($parent->subscribed($package->name)){
                return true;
            }
        }
        return false;
	}

    public function get_level()
    {
        return $this->attributes["level"];
    }

    public function get_permissions_checked()
    {

        $permissions = $this->get_permissions();
        $permissions = (!$permissions)?[]:$permissions;

        $perm_checked = [];
        foreach($permissions as $key => $value){
            $perm_checked[$key] = ($value) ? "checked" : "";
        }

        return $perm_checked;
    }

    public function is_superadmin()
    {
        return $this->attributes["superadmin"];
    }

    public function is_superuser()
    {
        return $this->attributes["superuser"];
    }

    public function is_superuser_on()
    {
        return $this->attributes["superuser_on"];
    }

    public function is_superuser_off()
    {
        return ( ! $this->attributes["superuser_on"] );
    }

    public function get_top_most_parent_id()
    {
        $users = explode('/', $this->path);

        return intval($users[0]);
    }

    public function get_top_most_user()
    {
        return User::find($this->get_top_most_parent_id());
    }

    public function get_top_most_stripe_id()
    {
        $user = $this->get_top_most_user();

        return $user->stripe_id;
    }

    public function get_package_is_on()
    {
        foreach(Package::all() as $package) {
            if ($this->onPlan($package->stripe_plan)) {
                return $package;
            }
        }
        return false;
    }

    public function compare_package_to_is_on($package_stripe_plan)
    {
        $packages = [];
        foreach(Package::all() as $package) {
            $packages[] = $package->stripe_plan;
        }

        $current = $this->get_package_is_on();
        $ci = array_search($current->stripe_plan, $packages);
        $pi = array_search($package_stripe_plan, $packages);

        if ($ci > $pi) return 1;
        else if ($ci == $pi) return 0;

        return -1;
    }
}