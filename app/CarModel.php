<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'make_id', 'user_id'
    ];

    public $timestamps = false;

    /**
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function make(){
        return $this->belongsTo('App\CarMake', 'make_id', 'id');
    }
}
