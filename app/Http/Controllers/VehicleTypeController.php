<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VehicleType as VehicleType;
use Session;


class VehicleTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicle_types = VehicleType::all();
        return view('car_params.vehicle_type.index')->with('vehicle_types', $vehicle_types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('car_params.vehicle_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        VehicleType::create([
            'name' => $request->get('name'),
        ]);

        Session::flash('flash_message', 'Vehicle type created successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/vehicle_type');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $vehicle_type = VehicleType::find((int)$id);

        return view('car_params.vehicle_type.edit')->with('vehicle_type', $vehicle_type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $vehicle_type = VehicleType::find((int)$id);
        $vehicle_type->name = $request->get('name');
        $vehicle_type->save();

        Session::flash('flash_message', 'Vehicle type updated successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/vehicle_type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vehicle_type = VehicleType::find($id);
        $vehicle_type->delete();

        Session::flash('flash_message', 'Vehicle type deleted successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/vehicle_type');
    }
}