<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FuelType as FuelType;
use Session;

class FuelTypeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fueltype = FuelType::all();
        return view('car_params.fuel_type.index')->with('fueltype', $fueltype);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('car_params.fuel_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required'
        ]);

        FuelType::create([
            'type' => $request->get('type')
        ]);

        Session::flash('flash_message', 'FuelType type created successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/fueltype');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $fueltype = FuelType::find((int)$id);

        return view('car_params.fuel_type.edit')->with('fueltype', $fueltype);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'type' => 'required',
        ]);

        $fueltype = FuelType::find((int)$id);
        $fueltype->type = $request->get('type');
        $fueltype->save();

        Session::flash('flash_message', 'FuelType type updated successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/fueltype');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fueltype = FuelType::find($id);
        $fueltype->delete();

        Session::flash('flash_message', 'Vehicle fueltype deleted successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/fueltype');
    }
}
