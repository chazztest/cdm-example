<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color as Color;
use Session;

class ColorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colors = Color::all();
        return view('car_params.color.index')->with('colors', $colors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('car_params.color.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        Color::create([
            'name' => $request->get('name'),
        ]);

        Session::flash('flash_message', 'Vehicle history created successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/color');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $color = Color::find((int)$id);

        return view('car_params.color.edit')->with('color', $color);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $color = Color::find((int)$id);
        $color->name = $request->get('name');
        $color->save();

        Session::flash('flash_message', 'Vehicle history updated successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/color');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $color = Color::find($id);
        $color->delete();

        Session::flash('flash_message', 'Vehicle history deleted successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/color');
    }
}
