<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Cashier\Cashier;
use Auth;
use Session;
use App;
use App\User;
use App\Package;
use App\Subscription;
use Carbon\Carbon;
use \Stripe\Stripe;
use \Stripe\InvoiceItem;
use \Stripe\Plan;


class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::all();

        $packages_array = [];

        foreach ($packages as $package) {
            $packages_array[$package->stripe_plan] = $package;
        }

        return view('subscriptions.index2', ['packages' => $packages_array]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function subscribe(Request $request)
    {
        $token = $request->get('stripeToken');
        $package_id = $request->get('package_id');

        $package = Package::find((int)$package_id);

        if(! isset($token) || ! isset($package_id)){
            if($request->ajax())
            {
                return response()
                    ->json(['success' => false,'flash_message' => 'Not subscribed. You were not charged. Please try again.', 'flash_type' => 'alert-danger'])
                    ->withCallback($request->input('callback'));
            }
        }

        Stripe::setApiKey(config('services.stripe.secret'));
        $all_plans = Plan::all();

        $plan_exists = false;
        foreach($all_plans['data'] as $plan)
        {
            if($package->stripe_plan == $plan['id']) {
                $plan_exists = true;
            }
        }

        if(! $plan_exists){
            if($request->ajax())
            {
                return response()
                    ->json(['success' => false,'flash_message' => 'Not subscribed. You were not charged. Please try again.', 'flash_type' => 'alert-danger'])
                    ->withCallback($request->input('callback'));
            }

            return redirect('/')->with('flash_message', 'Not subscribed. Plan doesn\'t exist on Stripe')->with('flash_type', 'alert-danger');
        }

        Cashier::useCurrency('gbp');

        $user = Auth::user();

        $trial_period = (!empty($package->trial_period)) ? intval($package->trial_period) : 0;

        $subscribe = $user->newSubscription($package->name, $package->stripe_plan);

        if ($trial_period > 0)
        {
            $subscribe->trialDays($trial_period);
        }

        $oSubscription = $subscribe->create($token);

        Subscription::where('id', $oSubscription->id)->update(['status_id' => 1]);


        Session::flash('flash_message', 'You have subscribed successfully.');
        Session::flash('flash_type', 'alert-success');

        if($request->ajax())
        {
            return response()
                ->json(['success' => true,'flash_message' => 'You have subscribed successfully.', 'flash_type' => 'alert-success'])
                ->withCallback($request->input('callback'));
        }



        return redirect('/');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function change()
    {
        $user = Auth::user()->get_top_most_user();

        $subscriptions = $user->subscriptions;

        if(null == $subscriptions || count($subscriptions) < 1)
        {
            Session::flash('flash_message', 'There are no subscriptions yet.');
            Session::flash('flash_type', 'alert-danger');

            return redirect('/');
        }

        $packages = Package::all();

        $allowed_upgrades = subscribeUp($packages, $subscriptions);

        $plan = $subscriptions[0]->stripe_plan;

        $packages_array = [];

        foreach ($packages as $package) {
            $package->active_button = false;
            if( count($allowed_upgrades) > 0 && isset($allowed_upgrades[$package->stripe_plan]))
            {
                $package->active_button = true;
            }


//            if ($user->is_superuser_on() && $user->is_superuser()) {
//                $package->active_button = true;
//            } else {
//
//                if ( $user->compare_package_to_is_on($package->stripe_plan) < 1 ) {
//                    $package->active_button = true;
//                } else {
//                    $package->active_button = false;
//                }
//            }

            $packages_array[$package->stripe_plan] = $package;
        }

        return view('subscriptions.change', ['packages' => $packages_array, 'plan' => $plan]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function change_proceed(Request $request)
    {

        $new_plan = $request->get('new_plan');

        $user = Auth::user()->get_top_most_user();

        $package = $user->get_package_is_on();

        Stripe::setApiKey(config('services.stripe.secret'));
        $all_plans = Plan::all();

        $plan_exists = false;
        foreach($all_plans['data'] as $plan)
        {
            if($new_plan == $plan['id']) {
                $plan_exists = true;
            }
        }

        if(!$plan_exists){
            return redirect('/')->with('flash_message', 'Not subscribed. Plan doesn\'t exist on Stripe')->with('flash_type', 'alert-danger');
        }


        $user->subscription($package->name)->swap($new_plan);

        if (App::environment('local')) {

            $plan = Package::where('stripe_plan', $new_plan)->first();
            Subscription::where('user_id', $user->id)
                        ->where('name', $package->name)
                        ->update(['name' => $plan->name]);
        }

        Session::flash('flash_message', 'Subscription has been changed successfully.');
        Session::flash('flash_type', 'alert-success');


        return redirect('/');

    }

    public function reactivate(){

        Stripe::setApiKey(config('services.stripe.secret'));

        $user = Auth::user()->get_top_most_user();

        $subscription = Subscription::where('user_id', $user->id )->first();

        if(!isset($subscription->stripe_id) || null == $subscription->stripe_id)
        {
            return back()
                ->with('flash_message', 'Subscription id was not found!')
                ->with('flash_type', 'alert-danger');
        }

        $stripe_sub = \Stripe\Subscription::retrieve($subscription->stripe_id);
        $stripe_sub->plan = $subscription->stripe_plan;
        $stripe_sub->save();

        $subscription->status_id = 0;
        $subscription->current_period_end = null;
        $subscription->save();

        return back()
            ->with('flash_message', 'Subscription has been reactivated successfully.' )
            ->with('flash_type', 'alert-success');

    }

    public function renew()
    {
        $user = Auth::user()->get_top_most_user();

        $packages = Package::all();

        $packages_array = [];


        foreach ($packages as $package) {
            $packages_array[$package->stripe_plan] = $package;
        }



        if ($user->stripe_id === null) {
            return view('subscriptions.index', ['packages' => $packages_array]);
        }


        return view('subscriptions.renew', ['packages' => $packages_array]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function renew_proceed(Request $request) {


        $new_plan = $request->get('new_plan');

        $user = Auth::user()->get_top_most_user();

        $package = $user->get_package_is_on();

        //$user->subscription($package->name)->swap($new_plan);

        if (App::environment('local')) {

            $plan = Package::where('stripe_plan', $new_plan)->first();
            Subscription::where('user_id', $user->id)
                        ->where('name', $package->name)
                        ->update(['name' => $plan->name]);
        }

        Session::flash('flash_message', 'Subscription has been changed successfully.');
        Session::flash('flash_type', 'alert-success');


        return redirect('/');
    }


    /**
     * @param \App\Http\Controllers\User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancel()
    {
        $auth_user = Auth::user();
        $user = $auth_user->get_top_most_user();

        Stripe::setApiKey(config('services.stripe.secret'));

        $subscription = Subscription::where('user_id', $user->id )->first();

        $stripe_user = \Stripe\Customer::retrieve($user->stripe_id);

        $current_period_end = null;
        if($subscription->stripe_id == $stripe_user->subscriptions['data'][0]['id']){
            $current_period_end = $stripe_user->subscriptions['data'][0]['current_period_end'];
        }

        if(null == $subscription->stripe_id)
        {
            return back()
                ->with('flash_message', 'Subscription id was not found!')
                ->with('flash_type', 'alert-danger');
        }

        $subscription->status_id = 2;
        $subscription->current_period_end = $current_period_end;
        //$subscription->current_period_end = date('Y-m-d H:i:s', $current_period_end);
        $subscription->save();



        $sub = \Stripe\Subscription::retrieve($subscription->stripe_id);
        if(null == $sub){
            return back()
                ->with('flash_message', 'Subscription already cancelled.')
                ->with('flash_type', 'alert-danger');
        }

        $sub->cancel(['at_period_end' => true]);

        return back()
            ->with('flash_message', 'Subscription was cancelled.')
            ->with('flash_type', 'alert-success');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function invoices()
    {
        Cashier::useCurrency('gbp');
        //$carbon = new Carbon();
        $user = Auth::user();

//        dd(get_class_methods($user));

        if(!$user->hasStripeId())
        {
            $stripe_id = $user->get_top_most_stripe_id();

            $user->stripe_id = $stripe_id;
        }

        if(isset($user->stripe_id))
        {
            $invoices = $user->invoices();
        }
        else
        {
            Session::flash('flash_message', 'User has no ID on Stripe.');
            Session::flash('flash_type', 'alert-danger');
            return back();
        }

//        dd($invoices);

        return view('subscriptions.invoices', ['invoices' => $invoices]);
    }

    public function download_invoice(Request $request,$invoiceId)
    {
        Cashier::useCurrencySymbol('£');

//        $inv = $request->user()->findInvoice($invoiceId);

        return $request->user()->downloadInvoice($invoiceId, [
                'vendor'  => 'Car Dealer Manager',
                'product' => 'Your Product',
            ]);
    }
}