<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CarMake as CarMake;
use App\VehicleType as VehicleType;
use Session;

class CarMakeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $v_type_id = $request->get('vehicle_type_id');
        if( !isset($v_type_id) ) $v_type_id = 1;

        $car_types_makes = VehicleType::where('id','=',$v_type_id)
            ->with('makes')->get();

        $car_types_makes = $car_types_makes->toArray();

        return view('car_params.car_make.index')
            ->with('car_makes', $car_types_makes[0]['makes'])
            ->with('vehicle_type_id', $v_type_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $vehicle_type = VehicleType::find((int)$id);

        return view('car_params.car_make.create')->with('vehicle_type', $vehicle_type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = VehicleType::find( (int)$request->get('vehicle_type_id'));

        if(!isset($type) || count($type) < 1){
            return back()
                ->with('flash_message', "Vehicle type not found")
                ->with('flash_type', 'alert-danger');
        }
        $this->validate($request, [
            'name' => 'required|unique_name_model:' . $request['vehicle_type_id'],
            'vehicle_type_id' => 'required|numeric'
        ]);

        CarMake::create([
            'name' => $request->get('name'),
            'vehicle_type_id' => (int)$request->get('vehicle_type_id'),
        ]);

        Session::flash('flash_message', 'Vehicle history created successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/car_make/' . (int)$request->get('vehicle_type_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $car_types_makes = VehicleType::where('id','=',$id)
            ->with('makes')->get();

        $car_types_makes = $car_types_makes->toArray();

        if(empty($car_types_makes)){
            return redirect('/car_make')
                ->with('flash_message', 'No vehicles found.')
                ->with('flash_type', 'alert-danger');
        }


        return view('car_params.car_make.show')
            ->with('vehicle_makes', $car_types_makes[0]['makes'])
            ->with('vehicle_type_name', $car_types_makes[0]['name'])
            ->with('vehicle_type_id', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $car_make = CarMake::find((int)$id);

        if(empty($car_make)){
            return redirect('/car_make')
                ->with('flash_message', 'No vehicles found.')
                ->with('flash_type', 'alert-danger');
        }

        return view('car_params.car_make.edit')->with('car_make', $car_make);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:car_makes'
        ]);

        $car_make = CarMake::find((int)$id);
        $car_make->name = $request->get('name');
        $car_make->save();

        Session::flash('flash_message', 'Vehicle history updated successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/car_make/' . $car_make->vehicle_type_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car_make = CarMake::find($id);
        $car_make->delete();

        Session::flash('flash_message', 'Vehicle history deleted successfully.');
        Session::flash('flash_type', 'alert-success');

//        return redirect('/car_make');
        return back()
            ->with('flash_message', 'Vehicle history deleted successfully.')
            ->with('flash_type', 'alert-success');

    }
}