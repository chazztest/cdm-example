<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transmission as Transmission;
use Session;

class TransmissionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transmission = Transmission::all();
        return view('car_params.transmission.index')->with('transmission', $transmission);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('car_params.transmission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required'
        ]);

        Transmission::create([
            'type' => $request->get('type')
        ]);

        Session::flash('flash_message', 'Transmission type created successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/transmission');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $car_transmission = Transmission::find((int)$id);

        return view('car_params.transmission.edit')->with('transmission', $car_transmission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'type' => 'required',
        ]);

        $transmission = Transmission::find((int)$id);
        $transmission->type = $request->get('type');
        $transmission->save();

        Session::flash('flash_message', 'Transmission type updated successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/transmission');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car_transmission = Transmission::find($id);
        $car_transmission->delete();

        Session::flash('flash_message', 'Vehicle transmission deleted successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/transmission');
    }
}
