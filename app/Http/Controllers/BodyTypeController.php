<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BodyType as BodyType;
use Session;

class BodyTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $body_types = BodyType::all();
        return view('car_params.body_type.index')->with('body_types', $body_types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('car_params.body_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required'
        ]);

        BodyType::create([
            'type' => $request->get('type')
        ]);

        Session::flash('flash_message', 'BodyType type created successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/body_type');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $body_type = BodyType::find((int)$id);

        return view('car_params.body_type.edit')->with('body_type', $body_type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'type' => 'required',
        ]);

        $body_type = BodyType::find((int)$id);
        $body_type->type = $request->get('type');
        $body_type->save();

        Session::flash('flash_message', 'BodyType type updated successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/body_type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $body_type = BodyType::find($id);
        $body_type->delete();

        Session::flash('flash_message', 'Vehicle body_type deleted successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/body_type');
    }
}