<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Permissions;
use Session;
use Mail;
use App\Location as Location;
use App\Mail\UserWelcome;
use App\Subscription;
use App\Stock;
use App\CarModel;
use Illuminate\Support\Facades\Hash;
use \Stripe\Stripe;

class UserController extends Controller
{
/*
    public function __construct()
    {
        $this->middleware('auth');
    }
*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $permissions = $user->get_permissions();

        $all_users = DB::table('users')
            ->where('path', 'like', $user->path.'%')
            ->get();

		return view('users.index', ['users' => $all_users, 'permissions' => $permissions]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        return view('users.show', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $locations = Location::all();
        $current_user = Auth::user();
        $current_user_level = $current_user['attributes']['level'];
        $user = DB::table("users")->where('id', $id)->first();

        if (!$user) {
            Session::flash('flash_message', 'User with the given id not exist.');
        	Session::flash('flash_type', 'alert-danger');
            return back();
        }
        $user_level = $user->level;

        $perms = unserialize($user->permissions);


        $available_perms = Permissions::get_available_perms();

        $available_levels = Permissions::get_available_levels_edit($current_user_level);

        $raw_available_perms = Permissions::get_raw_permissions_dawn($current_user->level);


        $groups = Permissions::get_ready_groups_depend_on_perms($perms);

        $raw_available_groups_templates = Permissions::get_raw_groups_templates_dawn($current_user->level);

        $perm_groups = Permissions::get_all_grouped_perms();
/*
        $user_location = DB::table('users_locations')
                                ->where('user_id', $id)
                                ->first();
        $user_location_id = (!empty($user_location->location_id))?$user_location->location_id:0;
*/
        $exploded_path = explode('/', $user->path);

        $query = DB::table('users')
                    ->where('path', 'like', $exploded_path[0] . '/%');

        if ($current_user->level != 'admin')
            $query = $query->where('id', '<>', $id);

        $location_managers = $query->get();


        return view('users.edit', [
                'user_id' => $user->id,
                'user_name' => $user->name,
                'user_last_name' => $user->last_name,
                'initials' => $user->initials,
                'user_email' => $user->email,
                'user_level' => $user_level,
                'user_perms' => $groups,//$perms,//
                'available_perms' => $available_perms,
                'available_levels' => $available_levels,
                'raw_available_perms' => $raw_available_groups_templates, //$raw_available_perms//
                'locations' => $locations,
                'user_location_id' => $user->location_id,
                'user_manager_id' => $user->manager_id,
                'location_managers' => $location_managers,
                'perm_groups' => $perm_groups
            ]);
    }


    public function user_edit(){

        $user = Auth::user();

        $user_perms = $user->get_permissions();

        $top_user = $user->get_top_most_user();

        $sub_id = $top_user->get_top_most_user()->id;

//        Stripe::setApiKey(config('services.stripe.secret'));
//        $stripe_user = \Stripe\Customer::retrieve($top_user->stripe_id);

        $top_user_subs = Subscription::where('user_id', $sub_id)->first();

        $sub_name = '';
        $sub_status = '';
        $sub_end_date = '';
        if(count($top_user_subs) > 0){
            $sub_name = $top_user_subs->name;
            $sub_status = $top_user_subs->status_id;
            $sub_end_date = date('d/m/Y H:i:s' , $top_user_subs->current_period_end);
        }



        return view('users.edit_user', [
            'user' => $user,
            'user_id' => $user->id,
            'user_name' => $user->name,
            'user_last_name' => $user->last_name,
            'initials' => $user->initials,
            'email' => $user->email,
            'subscription_name' => $sub_name,
            'sub_status' => $sub_status,
            'sub_end_date' => $sub_end_date,
            'user_perms' => $user_perms
        ]);
    }


    public function user_update(Request $request){

        $user = Auth::user();

        $update = $request->all();

        $rules = [
            'name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email'
        ];

        $data = [
            'name' => $update['name'],
            'last_name' => $update['last_name']
        ];

        if($request->get('email') !== $user->email){
            $rules['email'] = 'required|unique:users|email|max:255';

            $data['email'] = $request->get('email');
        }

        if( null != $request->get('old_password') || trim($request->get('old_password')) != ''){
            $rules['old_password'] = 'required|psw_match';
            $rules['password'] = 'required|confirmed|min:8';

            $data['password'] = Hash::make($request->get('password'));
        }

        $this->validate($request, $rules);


        DB::table('users')->where('id', $user->id)->update($data);

        /*        $user_id = $update['id'];
                $location_id = intval($update['location_id']);

                $exist = DB::table('users_locations')->where('user_id', $user_id)->first();

                if ( ! empty($exist)){
                    $query = DB::table('users_locations')->where('user_id', $user_id);
                    dd($query);
                    if ($location_id > 0) {
                        $query->update(['location_id' => $location_id]);
                    } else {
                        $query->delete();
                    }

                    if ($location_id > 0) {
                        DB::table('users_locations')->insert(['user_id' => $user_id, 'location_id' => $location_id]);
                    }
                }
        */

        Session::flash('flash_message', 'User successfully edited.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/users_view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $update = $request->all();

        $level = (!empty($update['level'])) ? $update['level'] : false;
        $groups = (!empty($update['permissions'])) ? $update['permissions'] : [];
        

        $permissions = Permissions::get_permissions_array_depend_on_groups($groups);

        $permissions_string = Permissions::get_permissions_string($permissions);

        $this->validate($request, [
            'name' => 'required',
        ]);

        $manager_id = (intval($update['manager_id']) > 0) ? $update['manager_id'] : null;
        $location_id = (intval($update['location_id']) > 0) ? $update['location_id'] : null;

        DB::table('users')
                ->where('id', $update['id'])
                ->update([
                        'name' => $update['name'],
                        'last_name' => $update['last_name'],
                        'initials' => $update['initials'],
                        'manager_id' => $manager_id,
                        'location_id' => $location_id,
                        'permissions' => $permissions_string,
                        'level' => $level,]);

/*        $user_id = $update['id'];
        $location_id = intval($update['location_id']);

        $exist = DB::table('users_locations')->where('user_id', $user_id)->first();

        if ( ! empty($exist)){
            $query = DB::table('users_locations')->where('user_id', $user_id);
            dd($query);
            if ($location_id > 0) {
                $query->update(['location_id' => $location_id]);
            } else {
                $query->delete();
            }

            if ($location_id > 0) {
                DB::table('users_locations')->insert(['user_id' => $user_id, 'location_id' => $location_id]);
            }
        }
*/

        Session::flash('flash_message', 'User successfully edited.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/users_view');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_id = Auth::id();

        if ($id == $user_id) {
            Session::flash('flash_message', 'You cannot delete yourself.');
        	Session::flash('flash_type', 'alert-danger');
            return back();
        }

        DB::table('users')->where('id', $id)->delete();

        Session::flash('flash_message', 'User successfully deleted.');
    	Session::flash('flash_type', 'alert-success');

        return redirect("/users_view");
    }

    public function destroy_user($id)
    {
        $user_id = Auth::id();

        if ($id == $user_id) {
            Session::flash('flash_message', 'You cannot delete yourself.');
            Session::flash('flash_type', 'alert-danger');
            return back();
        }

        CarModel::where('user_id', $id)->update(['user_id' => $user_id]);
        Stock::where('user_id', $id)->update(['user_id' => $user_id]);

        DB::table('users')->where('id', $id)->delete();

        Session::flash('flash_message', 'User successfully deleted.');
        Session::flash('flash_type', 'alert-success');

        return redirect("/users_view");
    }

    public function register_view() {
        $user = Auth::user();
        $locations = Location::all();

        $user_level = $user->get_level();
        $perms = Permissions::get_raw_permissions();
        $available_perms = Permissions::get_available_perms();
        $available_levels = Permissions::get_available_levels($user_level);

        $raw_available_groups_templates = Permissions::get_raw_groups_templates_dawn($user_level);

        $available_group_template = Permissions::get_first_group_template($raw_available_groups_templates);
        $available_groups = Permissions::get_available_groups_array();

        $perm_groups = Permissions::get_all_grouped_perms();

        $exploded_path = explode('/', $user->path);

        $location_managers = DB::table('users')
                    ->where('path', 'like', $exploded_path[0] . '/%')
                    ->get();

        return view('users.register', [
            'permissions' => $raw_available_groups_templates,//$perms,
            'available_perms' => $available_group_template,//$available_perms,
            'available_levels' => $available_levels,
            'locations' => $locations,
            'location_managers' => $location_managers,
            'perm_groups'   => $perm_groups,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = $request->all();

        $available_levels = Permissions::get_available_levels();


        $level = (!empty($store['level'])) ? $store['level'] : $available_levels[count($available_levels)-1];
        $groups = (!empty($store['permissions'])) ? $store['permissions'] : false;

        $permission_string = Permissions::get_perms_string_from_groups($groups);

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users|max:255'
        ]);

        $user = Auth::user();
        $cur_user_path = $user->path;

        $manager_id = (intval($store['manager_id']) > 0) ? $store['manager_id'] : null;
        $location_id = (intval($store['location_id']) > 0) ? $store['location_id'] : null;

        $id = DB::table('users')->insertGetId([
            'name' => $store['name'],
            'last_name' => $store['last_name'],
            'initials' => $store['initials'],
            'email' => $store['email'],
            'password' => bcrypt($store['password']),
            'permissions' => $permission_string,
            'level' => $level,
            'api_token' => str_random(60),
            'manager_id' => $manager_id,
            'location_id' => $location_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('users')
            ->where('id', $id)
            ->update(['path' => $cur_user_path . $id . '/']);

        /*if (intval($store['location_id']) > 0) {
            DB::table('users_locations')->insert([
                'user_id' => $id,
                'location_id' => intval($store['location_id'])
            ]);
        }*/

        /*
         * Welcome email, uncoment when need.
         */
        //Mail::to($store['email'])->send(new UserWelcome($store));

        Session::flash('flash_message', 'User added successfully');
    	Session::flash('flash_type', 'alert-success');

        return redirect()->action('UserController@index');
    }
}