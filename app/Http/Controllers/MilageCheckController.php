<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MilageCheck as MilageCheck;
use Session;

class MilageCheckController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $milages = MilageCheck::all();
        return view('car_params.milage_check.index')->with('milages', $milages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('car_params.milage_check.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        MilageCheck::create([
            'name' => $request->get('name'),
        ]);

        Session::flash('flash_message', 'Vehicle history created successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/milage');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $milage = MilageCheck::find((int)$id);

        return view('car_params.milage_check.edit')->with('milage', $milage);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $package = MilageCheck::find((int)$id);
        $package->name = $request->get('name');
        $package->save();

        Session::flash('flash_message', 'Vehicle history updated successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/milage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $milage = MilageCheck::find($id);
        $milage->delete();

        Session::flash('flash_message', 'Vehicle history deleted successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/milage');
    }
}
