<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CarHistory as CarHistory;
use Session;

class CarHistoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $car_hist = CarHistory::all();
        return view('car_params.history.index')->with('car_history', $car_hist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('car_params.history.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        CarHistory::create([
            'name' => $request->get('name'),
        ]);

        Session::flash('flash_message', 'Vehicle history created successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/car_history');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

     $car_history = CarHistory::find((int)$id);

     return view('car_params.history.edit')->with('car_history', $car_history);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $package = CarHistory::find((int)$id);
        $package->name = $request->get('name');
        $package->save();

        Session::flash('flash_message', 'Vehicle history updated successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/car_history');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car_history = CarHistory::find($id);
        $car_history->delete();

        Session::flash('flash_message', 'Vehicle history deleted successfully.');
        Session::flash('flash_type', 'alert-success');

        return redirect('/car_history');
    }
}
