<?php

namespace App\Http\Controllers\Webhooks;

use Exception;
use Illuminate\Http\Request;
use Stripe\Event as StripeEvent;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Response;
use DB;
use Log;
use App\User;
use App\Subscriptions;
use App\Package;
use App\Invoice;


class WebhookController extends Controller
{
    public function handleOrderPaymentSucceeded($payload)
    {
        Log::info('Order Payment Succeeded', ['details' => $payload]);

        return new Response('Webhook Handled', 200);
    }

    public function handleCustomerSubscriptionCreated($payload)
    {
        Log::info('Customer Subscription Created', ['details' => $payload]);

        return new Response('Webhook Handled', 200);
    }

    public function handleInvoicePaymentSucceeded($payload)
	{
		Log::info('Invoice Payment succeeded', ['details' => $payload]);

		return new Response('Webhook Handled', 200);
	}

    public function handleInvoicePaymentFailed(array $payload)
	{
		Log::info('Invoice Payment failed', ['details' => $payload]);

		return new Response('Webhook Handled', 200);
	}

    public function handleChargeSucceeded(array $payload)
	{
		Log::info('Charge Succeeded', ['details' => $payload]);

        $user_id = User::where('stripe_id', $payload['data']['object']['customer'])->first();
        $subscription = Subscription::where('user_id', $user_id)->orderBy('created_at', 'desc')->first();
        $package = Package::where('stripe_plan', $Subscription->stripe_plan)->first();

        Subscription::where('user_id', $user->id)
                    ->where('stripe_plan', $subscription->stripe_plan)
                    ->update(['name' => $package->name, 'status_id' => 1]);

		return new Response('Webhook Handled', 200);
	}


    /**
     * Handle a Stripe webhook call.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleWebhook(Request $request)
    {
        $payload = json_decode($request->getContent(), true);

        if (! $this->isInTestingEnvironment() && ! $this->eventExistsOnStripe($payload['id'])) {
            return new Response('wrong environment', 200);
        }

        $method = 'handle'.studly_case(str_replace('.', '_', $payload['type']));

        if (method_exists($this, $method)) {
            return $this->{$method}($payload);
        } else {
            return $this->missingMethod();
        }
    }

    /**
     * Handle a cancelled customer from a Stripe subscription.
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function handleCustomerSubscriptionDeleted(array $payload)
    {
        $user = $this->getUserByStripeId($payload['data']['object']['customer']);

        if ($user) {
            $user->subscriptions->filter(function ($subscription) use ($payload) {
                return $subscription->stripe_id === $payload['data']['object']['id'];
            })->each(function ($subscription) {
                $subscription->markAsCancelled();
            });
        }

        Log::info('Customer subscription deleted', ['details' => $payload]);

        return new Response('Webhook Handled', 200);
    }

    // deleted //

}
