<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,

        ],

        'api' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Session\Middleware\StartSession::class,
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'read' => \App\Http\Middleware\Permissions\Read::class,
        'update' => \App\Http\Middleware\Permissions\Update::class,
        'create' => \App\Http\Middleware\Permissions\Create::class,
        'delete' => \App\Http\Middleware\Permissions\Delete::class,
        'read_stock' => \App\Http\Middleware\Permissions\ReadStock::class,
        'add_stock' => \App\Http\Middleware\Permissions\AddStock::class,
        'edit_stock' => \App\Http\Middleware\Permissions\EditStock::class,
        'delete_stock' => \App\Http\Middleware\Permissions\DeleteStock::class,
        'cancel_order' => \App\Http\Middleware\Permissions\CancelOrder::class,
        'location_manager' => \App\Http\Middleware\Permissions\LocationManager::class,
        'billing' => \App\Http\Middleware\Permissions\Billing::class,
        'change_website' => \App\Http\Middleware\Permissions\ChangeWebsite::class,
        'confirm_sale' => \App\Http\Middleware\Permissions\ConfirmSale::class,
        'superadmin' => \App\Http\Middleware\CheckSuperAdmin::class,
        'admins' => \App\Http\Middleware\Admins::class,
        'not_subscribed' => \App\Http\Middleware\CheckIfNotSubscribed::class,
        'subscriptions' => \App\Http\Middleware\CheckSubscriptions::class,
        'view_invoices' =>  \App\Http\Middleware\Permissions\ViewInvoices::class,
        'subscription_management' =>  \App\Http\Middleware\Permissions\ViewInvoices::class,
    ];
}
