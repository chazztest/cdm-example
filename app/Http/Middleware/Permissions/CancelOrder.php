<?php

namespace App\Http\Middleware\Permissions;

use Closure;
use Auth;
use Session;

class CancelOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        $permissions = $user->get_permissions();

        $perm = (isset($permissions["cancel_order"]))? $permissions["cancel_order"] : false;

        if ( ! $perm) {
            Session::flash('flash_message', 'You don\'t have permission to cancel orders.');
        	Session::flash('flash_type', 'alert-danger');
            return back();
        }

        return $next($request);
    }
}
