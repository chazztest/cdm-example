<?php

namespace App\Http\Middleware\Permissions;

use Closure;
use Auth;
use Session;

class ConfirmSale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        $permissions = $user->get_permissions();
        
        $perm = (isset($permissions["confirm_sale"]))? $permissions["confirm_sale"] : false;

        if ( ! $perm) {
            Session::flash('flash_message', 'You don\'t have permission to confirm sale.');
        	Session::flash('flash_type', 'alert-danger');
            return back();
        }

        return $next($request);
    }
}
