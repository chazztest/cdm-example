<?php

namespace App\Http\Middleware\Permissions;

use Closure;
use Auth;
use Session;

class ChangeWebsite
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        $permissions = $user->get_permissions();

        $perm = (isset($permissions["change_website"]))? $permissions["change_website"] : false;

        if ( ! $perm) {
            Session::flash('flash_message', 'You don\'t have permission to change website.');
        	Session::flash('flash_type', 'alert-danger');
            return back();
        }

        return $next($request);
    }
}
