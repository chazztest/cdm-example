<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;
use App\Package;

class CheckIfNotSubscribed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guest()){
            return $next($request);
        }

        $user = Auth::user();

        $packages = Package::all();

        $parent = $user->get_top_most_user();

        foreach($packages as $package)
        {

            if ($user->subscribed($package->name))
            {
                return redirect('/');
            }
        }

        foreach($packages as $package) {
            if ($parent->subscribed($package->name))
            {
                return redirect('/');
            }
        }
        return $next($request);
    }


}