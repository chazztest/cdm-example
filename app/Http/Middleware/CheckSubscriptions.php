<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;
use App\Package;

class CheckSubscriptions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $packages = Package::all();

        $parent = $user->get_top_most_user();

        if ($user->is_superuser() && $user->is_superuser_on()) {
            return $next($request);
        }

        foreach($packages as $package){

            if ($user->subscribed($package->name)) {

                return $next($request);
            }
            //remove after testing xxx
//            return $next($request);
        }

        foreach($packages as $package) {
            if ($parent->subscribed($package->name)) {
                return $next($request);
            }
        }

        return redirect('/subscriptions');
    }
}
