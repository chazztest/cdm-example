<?php
function convertDateToEuro($date)
{
    if ($date === '00-00-0000' || $date === '0000-00-00' || $date == '') return $date;

    return date('Y-m-d', strtotime($date));
}

function convertDateToEn($date)
{
    if ($date === '00-00-0000' || $date === '0000-00-00' || $date == '' ) return $date;
    return date('d-m-Y', strtotime($date));
}

function isHTML($string)
{
    return preg_match('/^https?:/', $string);
}

function convertMediaLink($media_link)
{
    if(isset($media_link) and strlen(trim($media_link)) > 0)
    {
        $media_link = str_replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/', trim($media_link));
    }

    return $media_link;
}



function subscribeUp($packages, $subscriptions){
    $package_upgrade = [];

    foreach ($packages as $package) {
        if($subscriptions[0]->stripe_plan != $package->stripe_plan){
            $package_upgrade[$package->stripe_plan] = $package->price;
        }else{
            $package_price = $package->price;
        }
    }

    $package_upgrade = array_filter($package_upgrade, function($v) use ($package_price){
        return ($v > $package_price);

    });

    return $package_upgrade;
}