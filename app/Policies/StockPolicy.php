<?php

namespace App\Policies;

use App\User;
use App\Stock;
use Illuminate\Auth\Access\HandlesAuthorization;

class StockPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     * @return void
     */
    public function __construct() {
       //
    }

    public function managePages($user)
    {
        //return $user->hasRole(['Administrator', 'Content Editor']);
        return $user->email == 'kestutisa@gmail.com';
    }

    /**
     * Determine whether the user can view the stock.
     *
     * @param  \App\User  $user
     * @param  \App\Stock  $stock
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->email == 'kestutisa@gmail.com';
    }

    /**
     * Determine whether the user can create stocks.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the stock.
     *
     * @param  \App\User  $user
     * @param  \App\Stock  $stock
     * @return mixed
     */
    public function update(User $user, Stock $stock)
    {

    }

    /**
     * Determine whether the user can delete the stock.
     *
     * @param  \App\User  $user
     * @param  \App\Stock  $stock
     * @return mixed
     */
    public function delete(User $user, Stock $stock)
    {
        //
    }
}
