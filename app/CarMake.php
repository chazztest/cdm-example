<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarMake extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'vehicle_type_id'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function models(){
        return $this->hasMany('App\CarModel', 'make_id');
    }

    public function type(){
        return $this->belongsTo('App\VehicleType', 'vehicle_type_id', 'id');
    }
}
