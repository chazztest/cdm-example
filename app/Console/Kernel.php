<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        '\App\Console\Commands\Perms',
        '\App\Console\Commands\SubscriptionsClear',
        '\App\Console\Commands\SubscriptionsCreate',
        '\App\Console\Commands\SuperuserList',
        '\App\Console\Commands\SuperuserAdd',
        '\App\Console\Commands\SuperuserRemove',
        '\App\Console\Commands\SuperuserOn',
        '\App\Console\Commands\SuperuserOff',
        '\App\Console\Commands\MigrateKill',
        '\App\Console\Commands\ImportMakesModelsFromFiles',
        '\App\Console\Commands\ImportColors',
        '\App\Console\Commands\ImportBodyType',
        '\App\Console\Commands\ImportData',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
