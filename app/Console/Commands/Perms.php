<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Permissions;

class Perms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'perms:update_users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates all users permissions field. If been added new permission to class.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $users = DB::table("users")->get();

        $avail_perms = Permissions::get_available_perms();

        $temp_users =[];

        foreach ($users as $user) {
            $user_perms = unserialize($user->permissions);
            print_r($user_perms);
            $temp_perms = [];

            foreach ($avail_perms as $perm) {
                $temp_perm[$perm] = (isset($user_perms[$perm])?$user_perms[$perm]:false);
            }

            $perms_string = Permissions::get_permissions_string($temp_perm);

            //$temp_users[]=["id" => $user->id, "permissions" => $perms_string];

            DB::table("users")->where('id', $user->id)->update(['permissions' => $perms_string]);

        }

        //print_r($temp_users);

        $this->line(" ");
        $this->line("Users permissions were updated.");
        $this->line(" ");


    }
}
