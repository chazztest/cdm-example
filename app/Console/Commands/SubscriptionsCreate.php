<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \Stripe\Stripe;
use \Stripe\Plan;
use DB;
use App\Package;

class SubscriptionsCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriptions:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates subscription plan "Basic" in packages model and in stripe account.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = 'Basic';
        $stripe_plan = 'basic';
        $price = 20.00;
        $billing_period = 1;
        $trial_period = 15;


        Package::create([
            'name' => $name,
            'stripe_plan' => $stripe_plan,
            'price' => $price,
            'billing_period' => $billing_period,
            'trial_period' => $trial_period
        ]);

        $amount = $price * 100;

        Stripe::setApiKey(config('services.stripe.secret'));
        Plan::create(array(
            'id' => $stripe_plan,
            'name' => $name,
            'amount' => $amount,
            'interval' => 'month',
            'interval_count' => $billing_period,
            'trial_period_days' => $trial_period,
            'currency' => 'gbp',
        ));

        $this->info("Plan Basic was created.");
    }
}
