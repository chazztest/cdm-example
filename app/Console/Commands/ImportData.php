<?php

namespace App\Console\Commands;


use App\CarHistory;
use App\FuelType;
use App\MilageCheck;
use App\SaleType;
use App\Source;
use App\Status;
use App\Transmission;
use App\VatType;
use App\Warranty;
use File;
use App;
use DB;
use Illuminate\Console\Command;

class ImportData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import All Table Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    protected $tables = [
        'histories' => 'name',
        'fuel_types' => 'type',
        'sale_types' => 'value',
        'sources' => 'name',
        'milage_checks' => 'name',
        'statuses'  => 'name',
        'transmissions' => 'type',
        'vat_types'     => 'type',
        'warranties'    => 'name'
    ];

    protected $classNames = [
        'histories' => 'CarHistory',
        'fuel_types' => 'FuelType',
        'sale_types' => 'SaleType',
        'sources' => 'Source',
        'milage_checks' => 'MilageCheck',
        'statuses'  => 'Status',
        'transmissions' => 'Transmission',
        'vat_types'     => 'VatType',
        'warranties'    => 'Warranty'
    ];

    public function handle()
    {
        echo 'Importing data...';

        DB::statement("SET foreign_key_checks=0");
        CarHistory::truncate();

        DB::statement("SET foreign_key_checks=0");
        FuelType::truncate();

        DB::statement("SET foreign_key_checks=0");
        SaleType::truncate();

        DB::statement("SET foreign_key_checks=0");
        Source::truncate();

        DB::statement("SET foreign_key_checks=0");
        MilageCheck::truncate();

        DB::statement("SET foreign_key_checks=0");
        SaleType::truncate();

        DB::statement("SET foreign_key_checks=0");
        Status::truncate();

        DB::statement("SET foreign_key_checks=0");
        Transmission::truncate();

        DB::statement("SET foreign_key_checks=0");
        VatType::truncate();

        DB::statement("SET foreign_key_checks=0");
        Warranty::truncate();

        foreach($this->tables as $table_name => $type)
        {
            echo 'importing: ' . $table_name . ' ...';
            $this->extractFile($table_name);
            echo 'imported.';
        }

        echo 'Finished import.';
    }

    protected function extractFile($file_name)
    {
        $file = json_decode(File::get(storage_path('files/'.$file_name . '.txt')), true );

        $className = '\\App\\'. $this->classNames[$file_name];

        foreach ($file as $key => $item){
            $className::create([$this->tables[$file_name] => $item]);
        }
    }
}
