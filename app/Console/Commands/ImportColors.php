<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Color;
use File;
use DB;
use App;

class ImportColors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:colors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports all car colors';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'importing colors...';

        DB::statement("SET foreign_key_checks=0");
        Color::truncate();
        $this->extractFile('colors');

        echo 'Finished import.';

    }

    protected function extractFile($file_name)
    {
        $file = json_decode(File::get(storage_path('files/'.$file_name . '.txt')), true );

        foreach ($file as $key => $item) {
            $color = new Color();
            $color->name = $item;
            $color->save();
        }
    }
}
