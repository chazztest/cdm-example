<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class SubscriptionsClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriptions:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears all users subscriptions. Involves Users and Subscriptions tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('subscriptions')->delete();
        DB::table('users')->update([
            'stripe_id' => null,
            'card_brand' => null,
            'card_last_four' => null,
            'trial_ends_at' => null]);

        $this->info("All subscriptions were cleared.");
    }
}
