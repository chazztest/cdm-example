<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CarModel as CarModel;
use App\CarMake as CarMake;
use File;
use DB;
use App;

class ImportMakesModelsFromFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports all model make data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'importing...';

        DB::statement("SET foreign_key_checks=0");
        CarModel::truncate();
        DB::statement("SET foreign_key_checks=1");
        DB::statement("SET foreign_key_checks=0");
        CarMake::truncate();
        DB::statement("SET foreign_key_checks=1");

        $this->extractFile('car_makes_model');
        echo '1. First Imported. ';
        $this->extractFile('bike_makes_model');
        echo '2. Second Imported. ';
        $this->extractFile('van_makes_model');
        echo 'END Last Imported.';
    }

    protected function extractFile($file_name)
    {
        if($file_name == 'car_makes_model') $vehicle_type_id = 1;
        if($file_name == 'bike_makes_model') $vehicle_type_id = 2;
        if($file_name == 'van_makes_model') $vehicle_type_id = 3;

        $file = json_decode(File::get(storage_path('files/'.$file_name . '.txt')), true );

        foreach ($file as $key => $item) {

            $new_make = new CarMake();
            $new_make->name = $key;
            $new_make->vehicle_type_id = $vehicle_type_id;
            $new_make->save();
            $id = $new_make->id;

            $item = array_unique($item);

            foreach ($item as $k => $value) {
                $new_model = new CarModel();
                $new_model->name = $value;
                $new_model->make_id = $id;
                $new_model->save();
            }
        }

    }
}
