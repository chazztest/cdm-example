<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class SuperuserList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'superuser:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lists all users in system.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();

        foreach ($users as $user) {
            $this->line($user->id . " " . $user->name . " " . $user->last_name . " " . $user->superuser . " " . $user->superuser_on);
        }

        $this->info("All users have been listed.");
    }
}
