<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Schema;

class MigrateKill extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:kill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes all tables from database, including migration table as well.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        
        $tables = DB::select('SHOW TABLES');
        foreach($tables as $table) {
            $table_array = get_object_vars($table);
            Schema::drop($table_array[key($table_array)]);
        }

        $this->info('All tables were removed from database');
    }
}
