<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class SuperuserAdd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'superuser:add {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add user to superusers list.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user_id = $this->argument('user_id');
        $user = User::find($user_id);
        if ( ! $user) {
            $this->error("User by the given id not exist. Try to use superuser:list command to see all users.");
            return;
        }

        $user->superuser = 1;
        $user->save();
        $this->info("User " . $user->name . " " . $user->last_name . " added to superuser list.");
    }
}
