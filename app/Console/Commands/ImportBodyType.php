<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BodyType;
use File;
use DB;
use App;

class ImportBodyType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:body';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Body Type';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Importing body type...';

        DB::statement("SET foreign_key_checks=0");
        BodyType::truncate();
        $this->extractFile('body_type');

        echo 'Finished import.';
    }

    protected function extractFile($file_name)
    {
        $file = json_decode(File::get(storage_path('files/'.$file_name . '.txt')), true );

        foreach ($file as $key => $item){
            $body_type = new BodyType();
            $body_type->vehicle_type_id = 1;
            $body_type->type = $item;
            $body_type->save();
        }
    }
}
