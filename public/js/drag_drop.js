$(function() {
    $('.btn-file').on('click', function(e){
        e.stopPropagation();
    });

    // preventing page from redirecting
    $("html").on("dragover", function (e) {
        e.preventDefault();
        e.stopPropagation();
    });

    // preventing page from redirecting
    $("html").on("dragleave", function (e) {
        e.preventDefault();
        e.stopPropagation();
    });

    $("html").on("drop", function (e) {
        e.preventDefault();
        e.stopPropagation();

        $('.drag-photo-area p').removeClass('drag-over');
    });

    $(".upload-area, .vehicle-images").on('dragover', function(e){
        e.preventDefault();
        e.stopPropagation();
        console.log('dragover ....');
        $(this).addClass('dashed_border');

    });

    $(".upload-area, .vehicle-images").on("dragleave", function (e){
        $(this).removeClass('dashed_border');
    });

    $(".upload-area, .vehicle-images").on("drop", function (e){

        e.preventDefault();
        e.stopPropagation();

        $('.drag-photo-area p').removeClass('drag-over');

        $(this).removeClass('dashed_border');

        var files = e.originalEvent.dataTransfer.files;
        console.log('dropping multi files: ');
        console.log(files);

        dragDropPreload(files);
    });

    // Drag enter
    $('.upload-area').on('dragenter', function (e) {
        e.stopPropagation();
        e.preventDefault();
    });

    // Drag over
    $('.upload-area').on('dragover', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $('.drag-photo-area p').addClass('drag-over');
    });

    $('.add_vehicle_submit_btn').click(function(e){
        waitingDialog.show('Saving...', {dialogSize: 'sm', progressType: 'info'});
        e.preventDefault();
        $('.help-block').addClass('hidden');

        var eFormData = getAllFormData();
        if( eFormData){
            // console.log('reg num1 ',eFormData.get('reg_number'));
        }else{
            eFormData = new FormData($('#cdm-form-step-2')[0]);
        }
        console.log('External form data ',eFormData);

        var crud_url = "/stock/store";
        sendFormData(eFormData, crud_url);
    });

    $('.update_vehicle_submit_btn').click(function(e){
        waitingDialog.show('Saving...', {dialogSize: 'sm', progressType: 'info'});
        e.preventDefault();
        $('.help-block').addClass('hidden');

        var eFormData = getAllFormData();

        console.log('External form data ',eFormData);

        var stock_id = $(this).data('stockid');

        var crud_url = "/stock/update/" + stock_id;

        sendFormData(eFormData, crud_url, stock_id);
    });


});// end jQuery

var eFiles = {};
var count = {num: 0};

function getAllFormData() {
    var counter = 0;
    var formD = $('#cdm-form-step-2')[0];

    $("#file_input").value = '';

    console.log('gettin data Form Data:', formD);

    var fd = new FormData(formD);
    for(var key in eFiles){
        if(eFiles.hasOwnProperty(key) && eFiles[key] !== undefined){
            // fd.append('file_' + counter, eFiles[key]);
            fd.append('files['+ counter +']', eFiles[key]);
            counter += 1;
        }
    }
    return fd;
}

function addNewImg(reader, file){

    var output = $(`<div class="col-md-6 vehicle-block" data-imgid="` + count.num + `">
        <div class="car-image" style="background: url(` + reader.result + `) no-repeat center center; background-size: cover;">
            <div class="del-bin"></div>
        </div>
        <div class="car-image-head">Publish on</div>
            <div class="car-image-icons">
                <div class="car-icon-box icon-box-1"></div>
                <div class="car-icon-box icon-box-2"></div>
                <div class="car-icon-box icon-box-3"></div>
            </div>
     </div>`);

    eFiles[count.num] = file;
    count.num += 1;

    console.log('All files total :', eFiles);

    $('.vehicle-images').append(output).removeClass('hidden');
    removeVehicleImg(output);
}

function removeVehicleImg(imgBlock){
    $(imgBlock).find('.del-bin').click(function(){
        console.log('eFiles before remove ', eFiles);
        var imgid = $(imgBlock).data('imgid');
        console.log('img ID ', imgid);

        eFiles[imgid] = undefined;
        console.log('eFiles after remove ', eFiles);

        $(imgBlock).remove();
    });
    return true;
}

function sendFormData(formData, crud_url, stock_id = null){
    $.ajax({
        url: crud_url,
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (response){

            console.log('successful response----');
            console.log('RESP ', response);

            if(!response.success){
                loadHTMLErrors(response.errors);
            }else{
                if(!url) url = '';
                window.location.replace(url + '/stock/grid');
                window.location.href = url + '/stock/grid';
            }
        },
        error: function(response){
            var err = response.responseJSON;
            console.log(err);
        }
    });
}

function loadHTMLErrors(errors){
    if(errors){
        var counter = 0;
        $.each(errors, function(index, error){
            console.log(index,' ', error[0]);
            if(counter === 0){
                setTimeout(function (){
                    waitingDialog.hide();
                    scrollToError(index);
                }, 700);
            }

            $('#error_' + index + ' strong' ).text(error[0]);
            $('#error_' + index).removeClass('hidden');
            counter += 1;
        });
    }
}

jQuery.fn.scrollTo = function(elem, speed) {
    $(this).animate({
        scrollTop:  $(this).scrollTop() - $(this).offset().top + $(elem).offset().top - 80
    }, speed == undefined ? 500 : speed);
    return this;
};

function scrollToError(elementId){
    console.log(elementId);
    var elPos = $("#"+ elementId).position().top;
    $('#content-block').scrollTo("#"+ elementId, 700);
}

function preFileUpload(e){
    var fin = $('#file_input')[0];

    console.log('get first ', fin);

    var fileInput = fin.files;

    console.log('All files other ', fileInput);

    $.each(fileInput, function(index, file){
        (function(){
            var reader = new FileReader();

            console.log('fi loop ', file);
            console.log('fi loop [0] ', file);

            reader.onload = function(){
                addNewImg(reader, file);
            };

            reader.readAsDataURL(file);

        })(file);
    });
}

function dragDropPreload(files){
    $.each(files, function(index, file)
    {
        (function(){
            var reader = new FileReader();

            console.log('fi loop ', file);
            console.log('fi loop [0] ', file);

            reader.onprogress = function(){

            };

            reader.onload = function(){
                addNewImg(reader, file);
            };

            reader.readAsDataURL(file);

        })(file);
    });
}