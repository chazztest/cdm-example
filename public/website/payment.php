<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CDM website</title>

    <link rel="shortcut icon" href="/website/img/favicon.ico" type="image/x-icon" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="/website/css/web.css">

</head>
<body>

    <nav id="main-navigation" class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <!--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>-->
                <a class="navbar-brand" href="/website"><img src="/website/img/logo.jpg" alt="cdm-website logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="main-navigation-collapse">

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div id="main-content" class="container">
        <div class="row">
            <h1>Payment confirmation</h1>
            <br>
            <form action="/subscriptions" type="post">
                <div class="col-xs-6"><button name="submit" class="form-control btn btn-default">Confirm</button></div>
            </form>
            <div class="col-xs-6"><button id="cancel-btn" class="form-control btn btn-default">Cancel</button></div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <hr>
                    <p>
                        2017 - CDM | all rights reserved
                    </p>
                </div>
            </div>
        </div>
    </footer>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src='/website/js/web.js'></script>
</body>
</html>
