<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CDM website</title>

    <link rel="shortcut icon" href="/website/img/favicon.ico" type="image/x-icon" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="/website/css/web.css">

</head>
<body>

    <nav id="main-navigation" class="navbar navbar-default c-header">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <!--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>-->
                <a class="navbar-brand" href="/website"><img src="/website/img/logo.jpg" alt="cdm-website logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="main-navigation-collapse">

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/login">Login</a></li>
                    <li><a href="/register">Register</a></li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div id="main-content" class="container">
        <div class="row">
            <h1>Welcome to Car Dealer Manager website</h1>
            <br>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ex quo intellegitur officium medium quiddam esse, quod neque in bonis ponatur neque in contrariis. Iam id ipsum absurdum, maximum malum neglegi. Duo Reges: constructio interrete. At tu eadem ista dic in iudicio aut, si coronam times, dic in senatu. Conferam avum tuum Drusum cum C. Re mihi non aeque satisfacit, et quidem locis pluribus. Sed vos squalidius, illorum vides quam niteat oratio. Sin dicit obscurari quaedam nec apparere, quia valde parva sint, nos quoque concedimus; </p>
            <p>Igitur neque stultorum quisquam beatus neque sapientium non beatus. O magnam vim ingenii causamque iustam, cur nova existeret disciplina! Perge porro. Sed tamen est aliquid, quod nobis non liceat, liceat illis. Iubet igitur nos Pythius Apollo noscere nosmet ipsos. Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis. Si enim ad populum me vocas, eum. Fortasse id optimum, sed ubi illud: Plus semper voluptatis? Atque hoc loco similitudines eas, quibus illi uti solent, dissimillimas proferebas. </p>
            <p>Bona autem corporis huic sunt, quod posterius posui, similiora. Sed quanta sit alias, nunc tantum possitne esse tanta. Quamquam tu hanc copiosiorem etiam soles dicere. Qualem igitur hominem natura inchoavit? Bonum integritas corporis: misera debilitas. Qui enim voluptatem ipsam contemnunt, iis licet dicere se acupenserem maenae non anteponere. </p>
            <p>Hanc quoque iucunditatem, si vis, transfer in animum; Illa tamen simplicia, vestra versuta. Conclusum est enim contra Cyrenaicos satis acute, nihil ad Epicurum. Theophrastus mediocriterne delectat, cum tractat locos ab Aristotele ante tractatos? Quis est, qui non oderit libidinosam, protervam adolescentiam? Non potes, nisi retexueris illa. Praeterea sublata cognitione et scientia tollitur omnis ratio et vitae degendae et rerum gerendarum. Hoc ipsum elegantius poni meliusque potuit. </p>
            <p>An est aliquid, quod te sua sponte delectet? Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Cave putes quicquam esse verius. Facit igitur Lucius noster prudenter, qui audire de summo bono potissimum velit; Et quidem, inquit, vehementer errat; Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. Quid est igitur, cur ita semper deum appellet Epicurus beatum et aeternum? </p>
        </div>

    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <hr>
                    <p>
                        2017 - CDM | all rights reserved
                    </p>
                </div>
            </div>
        </div>
    </footer>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src='/website/js/web.js'></script>
</body>
</html>
